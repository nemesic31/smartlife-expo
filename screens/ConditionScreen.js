import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, ScrollView } from 'react-native';
import { Card, Button } from 'react-native-elements';
import { color } from 'react-native-reanimated';

export default class FirstScreen extends Component {
  static navigationOptions = {
    headerTruncatedBackTitle: 'กลับ',
    title: null,
  };
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.background}>
        <View style={styles.textBox}>
          <Text style={styles.textHeader}>ข้อตกลงการใช้งาน</Text>
          <Text style={styles.textHeader2}>การไฟฟ้านครหลวง</Text>
          <Text style={styles.textHeader3}>
            ขออนุญาติเข้าถึงข้อมูลของคุณ ซึ่งได้แก่ เลขที่บัตรประชาชน หมายเลขโทรศัพท์มือถือ อีเมล์
            และข้อมูลมิเตอร์ไฟฟ้าของคุณ
          </Text>
        </View>
        <View style={{ marginBottom: 15 }}>
          <Button
            buttonStyle={{
              marginRight: 10,
              marginLeft: 10,
              marginBottom: 0,
              borderRadius: 10,
              backgroundColor: '#ECECEC',
              width: 390,
            }}
            title="ยอมรับ"
            titleStyle={{ color: '#395DE2' }}
            type="solid"
            onPress={() => navigate('Register')}
          />
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  background: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'space-between',
    flex: 2,
    flexDirection: 'column',
  },
  textBox: { width: '95%', marginTop: 15 },
  textHeader: {
    fontSize: 28,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  textHeader2: {
    fontWeight: 'bold',
    fontSize: 18,
    marginBottom: 10,
  },
  textHeader3: {
    fontSize: 18,
    marginBottom: 10,
  },
});
