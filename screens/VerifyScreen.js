import React, { Component } from 'react';
import { Text, View, StyleSheet, Platform } from 'react-native';
import OTPInputView from '@twotalltotems/react-native-otp-input';
import { Button } from 'react-native-elements';
import { KeyboardAvoidingView } from 'react-native';

export default class VerifyScreen extends Component {
  static navigationOptions = {
    title: '',
  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.background}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : null}
          enabled={Platform.OS === 'ios' ? true : false}
          keyboardVerticalOffset={Platform.OS === 'ios' ? 60 : 0}>
          <View style={{ marginTop: 15 }}>
            <Text style={styles.textHeader}>กรอกรหัส OTP 6 หลัก</Text>
            <Text style={styles.textHeader2}>
              ระบบได้ส่งรหัสไปยังหมายเลข 099-789-1234{'\n'}กรุณากรอกรหัส 6 หลัก ที่ได้รับจาก SMS
            </Text>
            <View style={{ alignItems: 'center', justifyContent: 'center' }}>
              <OTPInputView
                style={{ width: '80%', height: '50%' }}
                pinCount={6}
                // code={this.state.code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                // onCodeChanged = {code => { this.setState({code})}}
                autoFocusOnLoad
                codeInputFieldStyle={styles.underlineStyleBase}
                codeInputHighlightStyle={styles.underlineStyleHighLighted}
                onCodeFilled={(code) => navigate('Home')}
              />
            </View>
            <View style={{ marginTop: 45, alignItems: 'center' }}>
              <Button title="ส่งอีกครั้ง" titleStyle={{ color: '#395DE2' }} type="clear" />
            </View>
          </View>
        </KeyboardAvoidingView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  background: {
    backgroundColor: '#fff',
    height: '100%',
    alignItems: 'center',
    width: '100%',
    flex: 1,
  },
  textBox: { alignItems: 'center', marginTop: 15 },
  textHeader: {
    fontSize: 34,
    fontWeight: 'bold',
    marginBottom: 8,
    marginTop: 20,
    marginLeft: 20,
  },
  textHeader2: {
    fontSize: 18,
    marginBottom: 10,
    marginLeft: 20,
    color: '#8c8c8c',
  },

  borderStyleBase: {
    width: 30,
    height: 45,
  },
  borderStyleHighLighted: {
    borderColor: '#000',
  },
  underlineStyleBase: {
    width: 30,
    height: 45,
    borderWidth: 0,
    borderBottomWidth: 1,
  },
  underlineStyleHighLighted: {
    borderColor: '#000',
    color: '#000',
  },
});
