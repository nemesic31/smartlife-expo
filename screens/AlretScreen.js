import React, { Component } from 'react';
import { ScrollView, Text, View, StyleSheet } from 'react-native';
import { Card, ListItem, Button } from 'react-native-elements';
import Icons from 'react-native-vector-icons/MaterialIcons';

export default class AlretScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: '',
    headerStyle: { height: 90 },
    headerRight: () => (
      <View
        style={{
          justifyContent: 'space-around',
          flex: 1,
          alignItems: 'flex-end',
        }}>
        {Platform.OS !== 'ios' && (
          <Icons
            style={{ marginRight: 8, marginTop: 8 }}
            name="notifications"
            size={30}
            color="#007aff"
          />
        )}
        <Button
          style={{ marginRight: 16, marginBottom: 8 }}
          title="ดูประวัติการแจ้ง"
          type="clear"
          containerStyle={{ width: '100%' }}
          onPress={() => navigation.navigate('HistoryAlret')}
        />
      </View>
    ),
    headerLeft: () => (
      <View style={{ justifyContent: 'flex-end', flex: 1, flexDirection: 'column' }}>
        <Text style={{ fontSize: 34, fontWeight: 'bold', marginLeft: 16, marginBottom: 8 }}>
          แจ้งเหตุ
        </Text>
      </View>
    ),
  });
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={{ height: '100%', backgroundColor: '#fff' }}>
        <ScrollView ontentInsetAdjustmentBehavior="automatic">
          <ListItem
            title="ระบุตำแหน่งที่ต้องการแจ้งเตือน"
            bottomDivider
            titleStyle={styles.textHeader}
          />
          <ListItem
            leftIcon={{ name: 'home', type: 'entypo', color: '#ACADB0' }}
            rightIcon={{ name: 'navigate-next', color: '#ACADB0' }}
            title="ตำแหน่งบ้านของฉัน"
            bottomDivider
            onPress={() => navigate('SelectAlret')}
          />

          <ListItem
            leftIcon={{ name: 'user', type: 'entypo', color: '#ACADB0' }}
            rightIcon={{ name: 'navigate-next', color: '#ACADB0' }}
            title="ตำแหน่งปัจจุบันของฉัน"
            bottomDivider
            onPress={() => navigate('SelectAlretPublic')}
          />
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  textHeader: {
    fontWeight: 'bold',
    fontSize: 20,
  },
  listItemStyle: {
    margin: 5,
    borderBottomColor: '#E4933E',
  },
});
