import React, { Component } from 'react';
import { View, Text, StyleSheet, StatusBar } from 'react-native';
import { Card, Button } from 'react-native-elements';

export default class FirstScreen extends Component {
  static navigationOptions = { headerShown: false };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.background}>
        <StatusBar barStyle="dark-content" backgroundColor="white" />
        <Text style={styles.textHeader}>MEA</Text>

        <Text style={styles.textHeader2}>Smart Life</Text>
        <Button
          buttonStyle={{
            marginBottom: 14,
            borderRadius: 10,
            backgroundColor: '#ECECEC',
            width: 400,
          }}
          title="ลงทะเบียน"
          titleStyle={{ color: '#395DE2' }}
          type="solid"
          onPress={() => navigate('Condition')}
        />
        <Button
          buttonStyle={{
            width: 400,
          }}
          title="เข้าสู่ระบบ"
          titleStyle={{ color: '#395DE2' }}
          type="clear"
          onPress={() => navigate('Home')}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  background: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  textHeader: {
    fontSize: 40,
    fontWeight: 'bold',
    marginBottom: 44,
  },
  textHeader2: {
    fontWeight: 'bold',
    fontSize: 34,
    marginBottom: 50,
  },
});
