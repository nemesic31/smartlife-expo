import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, ScrollView } from 'react-native';
import { Card, ListItem } from 'react-native-elements';
import Icons from 'react-native-vector-icons/MaterialIcons';

export default class DetailHistoryAlretScreen extends Component {
  static navigationOptions = {
    title: 'รายละเอียด',
  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.background}>
        <ScrollView ontentInsetAdjustmentBehavior="automatic">
          <Card>
            <ListItem
              title="25/10/2562 4:30 PM"
              subtitle="แจ้งพบไฟฟ้าดับ"
              bottomDivider
              titleStyle={{ fontSize: 13, marginBottom: 20 }}
              subtitleStyle={{ fontSize: 22 }}
            />
            <ListItem title="การแจ้งสำเร็จ" bottomDivider titleStyle={{ fontSize: 17 }} />
            <Text style={styles.textHeader}>ตำแหน่งที่แจ้ง</Text>
            <Text style={styles.textHeader}>
              อาคารวัฒนวิภาส เลขที่ 1192 ถนนพระรามที่ ๔ แขวงคลองเตย เขตคลองเตย กรุงเทพมหานคร 10110
            </Text>
          </Card>
          <Card>
            <ListItem
              title="25/10/2562 4:31 PM"
              subtitle="เจ้าหน้าที่รับเรื่อง"
              bottomDivider
              titleStyle={{ fontSize: 13, marginBottom: 20 }}
              subtitleStyle={{ fontSize: 22 }}
            />
            <ListItem title="กำลังดำเนินการ" bottomDivider titleStyle={{ fontSize: 17 }} />
            <Text style={styles.textHeader}>เจ้าหน้าที่ตรวจสอบข้อมูลเบื้องต้น</Text>
          </Card>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  background: {
    height: '100%',

    width: '100%',
  },
  textHeader: {
    fontSize: 15,
    marginLeft: 15,
  },
});
