import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, ScrollView, StatusBar } from 'react-native';
import { Card, Button } from 'react-native-elements';
import Icons from 'react-native-vector-icons/MaterialIcons';
import Icon from 'react-native-vector-icons/Ionicons';

export default class HistoryAlertScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'ประวัติการแจ้ง',
    headerLeft: () => (
      <View style={{ justifyContent: 'flex-end', flex: 1, flexDirection: 'column' }}>
        <Icon
          name="ios-arrow-back"
          style={{ fontSize: 20, color: '#007AFF', marginLeft: 10, marginBottom: 10 }}
          onPress={() => navigation.navigate('Home')}
        />
      </View>
    ),
  });

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.background}>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <ScrollView ontentInsetAdjustmentBehavior="automatic">
          <TouchableOpacity onPress={() => navigate('DetailHistoryAlret')}>
            <Card containerStyle={{ borderColor: '#747477', borderRadius: 10 }}>
              <View style={{ flexDirection: 'row', width: '100%' }}>
                <Text style={styles.textHeader}>แจ้งพบไฟฟ้าดับตำแหน่งปัจจุบัน</Text>
                <Text style={styles.textHeader4}>25/10/62</Text>
              </View>

              <View style={{ flexDirection: 'row', width: '100%' }}>
                <Text style={styles.textHeader2}>สถานะการแจ้งเรื่อง </Text>

                <Text style={styles.textHeader3}>
                  สำเร็จ
                  <Icons name="navigate-next" style={{ fontSize: 15, color: '#ACADB0' }} />
                </Text>
              </View>
            </Card>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigate('DetailHistoryAlret')}>
            <Card containerStyle={{ borderColor: '#747477', borderRadius: 10 }}>
              <View style={{ flexDirection: 'row', width: '100%' }}>
                <Text style={styles.textHeader}>แจ้งพบไฟฟ้าดับSky's Home</Text>
                <Text style={styles.textHeader4}>25/10/62</Text>
              </View>

              <View style={{ flexDirection: 'row', width: '100%' }}>
                <Text style={styles.textHeader2}>สถานะการแจ้งเรื่อง </Text>

                <Text style={styles.textHeader3}>
                  สำเร็จ
                  <Icons name="navigate-next" style={{ fontSize: 15, color: '#ACADB0' }} />
                </Text>
              </View>
            </Card>
          </TouchableOpacity>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  background: {
    backgroundColor: '#fff',
    height: '100%',

    width: '100%',
  },
  textHeader: {
    fontSize: 13,
    marginBottom: 5,
    width: '50%',
  },
  textHeader4: {
    fontSize: 13,
    marginBottom: 5,
    width: '50%',
    textAlign: 'right',
  },
  textHeader2: {
    fontSize: 15,
    marginBottom: 15,
    marginTop: 25,
    width: '50%',
  },
  textHeader3: {
    fontSize: 15,
    marginBottom: 15,
    marginTop: 25,
    width: '50%',
    textAlign: 'right',
  },
});
