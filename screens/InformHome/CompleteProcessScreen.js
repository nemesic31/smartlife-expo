import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import Icons from 'react-native-vector-icons/SimpleLineIcons';

export default class CompleteProcessScreen extends Component {
  static navigationOptions = { headerShown: false };
  render() {
    const { navigate } = this.props.navigation;
    setTimeout(() => {
      navigate('Alret');
    }, 1500);
    return (
      <View style={styles.background}>
        <Icons name="check" style={{ fontSize: 100, marginBottom: 20 }} />
        <Text style={styles.textHeader}>ดำเนินการสำเร็จ</Text>
        <Text style={styles.textHeader2}>เจ้าหน้าที่ดำเนินการตรวจสอบและติดต่อกลับ</Text>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  background: {
    backgroundColor: '#fff',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  textHeader: {
    fontWeight: 'bold',
    fontSize: 45,
    marginBottom: 5,
  },
  textHeader2: {
    fontSize: 17,
  },
});
