import React, { Component } from 'react';
import { View, StyleSheet, Dimensions, Animated, Text } from 'react-native';
import { Button } from 'react-native-elements';
import MapView, { Marker, ProviderPropType, Callout } from 'react-native-maps';
import Geocoder from 'react-native-geocoding';
import SlidingUpPanel from 'rn-sliding-up-panel';
import { notificationManager } from '../NotificationManager';

const { width, height } = Dimensions.get('window');

const ASPECT_RATIO = width / height;
const LATITUDE = 37.78825;
const LONGITUDE = -122.4324;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
let id = 0;

class MapScreen extends Component {
  static navigationOptions = { headerShown: false };

  _draggedValue = new Animated.Value(120);
  constructor(props) {
    super(props);

    this.localNotify = {};

    this.state = {
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      address: null,
      markers: [],
    };
  }

  onMapPress(e) {
    this.setState({
      markers: [
        ...this.state.markers,
        {
          coordinate: e.nativeEvent.coordinate,
          key: id++,
          color: 'red',
        },
      ],
    });
  }

  componentDidMount = () => {
    Geocoder.from(LATITUDE, LONGITUDE)
      .then((json) => {
        this.setState({ address: json.results[2].formatted_address });
      })
      .catch((error) => console.warn(error));

    this.localNotify = notificationManager;
    this.localNotify.configure();
  };

  onPressSendNotification = () => {
    this.localNotify.showNotification(1, 'SmartLife', 'แจ้งเหตุ สำเร็จ');

    this.props.navigation.navigate('CompleteProcess');
  };

  render() {
    const { address } = this.state;

    return (
      <View style={styles.background}>
        <View
          style={{
            width: '100%',
            height: '100%',
            justifyContent: 'flex-end',
          }}>
          <MapView
            provider={this.props.provider}
            style={styles.map}
            initialRegion={this.state.region}>
            <Marker coordinate={this.state.region}>
              <Callout style={{ width: 200 }}>
                <View>
                  <Text style={{ fontWeight: 'bold' }}>{address}</Text>
                </View>
                <View style={{ marginTop: 8 }}>
                  <Text>latitude: {this.state.region.latitude}</Text>
                  <Text>longitude:{this.state.region.longitude}</Text>
                </View>
              </Callout>
            </Marker>
          </MapView>
          <SlidingUpPanel
            ref={(c) => (this._panel = c)}
            draggableRange={{ top: 220, bottom: 120 }}
            animatedValue={this._draggedValue}
            height={height + 120}
            friction={1.5}
            snappingPoints={[220]}
            showBackdrop={false}>
            <View style={styles.panel}>
              <View style={styles.panelHeader}>
                <Animated.View>
                  <Text style={styles.textTitleHeader}>แจ้งพบไฟฟ้าดับ</Text>
                  <Text style={{ fontSize: 14, color: '#999' }}>{address}</Text>
                </Animated.View>
              </View>
              <View style={styles.containerPanel}>
                <Button
                  title="แจ้ง"
                  type="solid"
                  buttonStyle={{
                    borderRadius: 5,
                    marginLeft: 10,
                    marginRight: 10,
                    marginBottom: 25,
                  }}
                  onPress={this.onPressSendNotification}
                />
              </View>
            </View>
          </SlidingUpPanel>
        </View>
      </View>
    );
  }
}

MapScreen.propTypes = {
  provider: ProviderPropType,
};

const styles = StyleSheet.create({
  containerPanel: {
    // height: '100%',
    backgroundColor: '#fff',
    justifyContent: 'space-between',
  },
  panel: {
    flex: 1,
    backgroundColor: 'white',
    position: 'relative',
  },
  panelHeader: {
    height: 120,
    backgroundColor: '#fbfbfd',
    padding: 24,
  },
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  bubble: {
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20,
  },
  latlng: {
    width: 200,
    alignItems: 'stretch',
  },
  button: {
    width: 80,
    paddingHorizontal: 12,
    alignItems: 'center',
    marginHorizontal: 10,
  },
  buttonContainer: {
    flexDirection: 'row',
    marginVertical: 20,
    backgroundColor: 'transparent',
  },
  background: {
    height: '100%',
    backgroundColor: '#fff',
  },
});
export default MapScreen;
