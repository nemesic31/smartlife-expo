import React, { Component } from 'react';
import { View, Text, StyleSheet, KeyboardAvoidingView, Platform, StatusBar } from 'react-native';
import { Input, Button } from 'react-native-elements';

export default class RegisterScreen extends Component {
  static navigationOptions = {
    title: '',
  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.background}>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : null}
          enabled={Platform.OS === 'ios' ? true : false}
          keyboardVerticalOffset={Platform.OS === 'ios' ? 60 : 0}>
          <View style={styles.containner}>
            <View>
              <Text style={styles.textHeader}>ลงทะเบียน</Text>
              <View style={{ width: '95%', alignSelf: 'center' }}>
                <Input
                  placeholder="เลขประจำตัวประชาชน"
                  placeholderTextColor="gray"
                  errorStyle={{ color: 'red' }}
                  errorMessage="กรุณากรอกข้อมูลให้ถูกต้อง"
                />
                <Input
                  placeholder="เบอร์โทรศัพท์"
                  placeholderTextColor="gray"
                  errorStyle={{ color: 'red' }}
                  errorMessage="กรุณากรอกข้อมูลให้ถูกต้อง"
                />
              </View>
            </View>
            <View style={styles.button}>
              <Button
                buttonStyle={{
                  marginRight: 10,
                  marginLeft: 10,
                  marginBottom: 0,
                  borderRadius: 10,
                  backgroundColor: '#ECECEC',
                  width: 390,
                }}
                title="ต่อไป"
                titleStyle={{ color: '#395DE2' }}
                type="solid"
                onPress={() => navigate('Verify')}
              />
            </View>
          </View>
        </KeyboardAvoidingView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  background: {
    backgroundColor: '#fff',
    height: '100%',
    alignItems: 'center',
    width: '100%',
    flex: 1,
  },
  button: { flex: 1, justifyContent: 'flex-end', marginBottom: 15 },
  textHeader: {
    fontSize: 34,
    fontWeight: 'bold',
    marginBottom: 40,
    marginTop: 15,
    marginLeft: 20,
  },
  textHeader2: {
    fontWeight: 'bold',
    fontSize: 20,
    marginBottom: 10,
  },
});
