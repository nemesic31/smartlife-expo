import React, { Component } from 'react';
import { Text, View, StyleSheet, ScrollView, StatusBar } from 'react-native';
import { Card } from 'react-native-elements';
import Icons from 'react-native-vector-icons/MaterialIcons';

export default class NewsScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: '',
    headerStyle: { height: 90 },
    headerRight: () => (
      <View
        style={{
          justifyContent: 'flex-start',
          flex: 1,
          alignItems: 'flex-end',
        }}>
        <Icons
          style={{ marginRight: 8, marginTop: 8 }}
          name="notifications"
          size={30}
          color="#007aff"
        />
      </View>
    ),
    headerLeft: () => (
      <View style={{ justifyContent: 'flex-end', flex: 1, flexDirection: 'column' }}>
        <Text style={{ fontSize: 34, fontWeight: 'bold', marginLeft: 16, marginBottom: 8 }}>
          สวัสดี
        </Text>
      </View>
    ),
  });

  render() {
    return (
      <View style={styles.background}>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <ScrollView ontentInsetAdjustmentBehavior="automatic" style={{ height: '100%' }}>
          <Card containerStyle={{ backgroundColor: '#A7A6A5', borderRadius: 10, height: 400 }}>
            <Text style={{ marginBottom: 5, fontSize: 20, color: '#fff' }}>
              โปรโมชั่นในเดือนนี้
            </Text>
            <Text style={{ marginBottom: 5, fontSize: 30, fontWeight: 'bold', color: '#fff' }}>
              จ่ายค่าไฟออนไลน์
            </Text>
            <Text style={{ marginBottom: 5, fontSize: 30, fontWeight: 'bold', color: '#fff' }}>
              แล้วชวนคนรู้ใจไปดูหนังฟรี
            </Text>
          </Card>
          <Card containerStyle={{ backgroundColor: '#A7A6A5', borderRadius: 10, height: 400 }}>
            <Text style={{ marginBottom: 5, fontSize: 20, color: '#fff' }}>
              โปรโมชั่นในเดือนนี้
            </Text>
            <Text style={{ marginBottom: 5, fontSize: 30, fontWeight: 'bold', color: '#fff' }}>
              จ่ายค่าไฟออนไลน์
            </Text>
            <Text style={{ marginBottom: 5, fontSize: 30, fontWeight: 'bold', color: '#fff' }}>
              แล้วชวนคนรู้ใจไปดูหนังฟรี
            </Text>
          </Card>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  background: {
    backgroundColor: '#fff',
  },
});
