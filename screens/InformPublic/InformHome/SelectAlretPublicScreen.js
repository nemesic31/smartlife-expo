import React, { Component } from 'react';
import { Text, View, StyleSheet, StatusBar, ScrollView } from 'react-native';
import { ListItem } from 'react-native-elements';
import Icon from 'react-native-vector-icons/Ionicons';
export default class SelectAlretPublicScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'เลือกการแจ้งเหตุ',
    headerLeft: () => (
      <View style={{ justifyContent: 'flex-end', flex: 1, flexDirection: 'column' }}>
        <Icon
          name="ios-arrow-back"
          style={{ fontSize: 20, color: '#007AFF', marginLeft: 10, marginBottom: 10 }}
          onPress={() => navigation.navigate('Alret')}
        />
      </View>
    ),
  });
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.background}>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <ScrollView ontentInsetAdjustmentBehavior="automatic">
          <Text style={styles.textHeader}> แจ้งเหตุที่ตำแหน่งปัจจุบันของคุณ</Text>
          <Text style={styles.textHeader2}>
            884, 12-13 ถนน พระราม 9 แขวงห้วยขวาง เขตห้วยขวาง กรุงเทพมหานคร 10310
          </Text>
          <Text style={styles.textHeader3}>เหตุไฟฟ้าขัดข้อง</Text>
          <ListItem
            title="พบไฟฟ้าดับ"
            bottomDivider
            rightIcon={{ name: 'navigate-next', color: '#ACADB0' }}
            onPress={() => navigate('MapPublic')}
          />
          <Text style={styles.textHeader3}>เหตุร้องเรียน</Text>
          <ListItem
            title="สายไฟต่ำ/ห้อย"
            bottomDivider
            rightIcon={{ name: 'navigate-next', color: '#ACADB0' }}
            onPress={() => navigate('MapPublic')}
          />
          <ListItem
            title="ไฟฟ้าดับสาธารณะ"
            bottomDivider
            rightIcon={{ name: 'navigate-next', color: '#ACADB0' }}
            onPress={() => navigate('MapPublic')}
          />

          <Text style={styles.textHeader3}>เหตุอิ่นๆ</Text>
          <ListItem
            title="ขอความช่วยเหลือ"
            bottomDivider
            rightIcon={{ name: 'navigate-next', color: '#ACADB0' }}
            onPress={() => navigate('MapPublic')}
          />
          <ListItem
            title="อื่นๆ"
            bottomDivider
            rightIcon={{ name: 'navigate-next', color: '#ACADB0' }}
            onPress={() => navigate('MapPublic')}
          />
          <View style={{ margin: 20 }}></View>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  background: {
    height: '100%',
  },
  textHeader: {
    fontSize: 25,
    fontWeight: 'bold',
    marginBottom: 15,
    marginTop: 20,
    marginLeft: 10,
  },
  textHeader2: {
    fontSize: 15,
    marginBottom: 40,
    marginLeft: 15,
    color: '#7D7E82',
  },
  textHeader3: {
    fontSize: 15,
    marginLeft: 15,
    marginBottom: 10,
    marginTop: 20,
    color: '#7D7E82',
  },
});
