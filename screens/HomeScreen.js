import React, { Component } from 'react';
import { View, Text, StatusBar, StyleSheet, ScrollView, Platform } from 'react-native';
import { Card, Button } from 'react-native-elements';
import Icons from 'react-native-vector-icons/MaterialIcons';

export default class HomeScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: '',
    headerStyle: { height: 90 },
    headerRight: () => (
      <View
        style={{
          justifyContent: 'space-around',
          flex: 1,
          alignItems: 'flex-end',
        }}>
        {Platform.OS !== 'ios' && (
          <Icons
            style={{ marginRight: 8, marginTop: 8 }}
            name="notifications"
            size={30}
            color="#007aff"
          />
        )}
        <Button
          style={{ marginRight: 16, marginBottom: 8 }}
          title="เพิ่มมิเตอร์ไฟฟ้า"
          type="clear"
          containerStyle={{ width: '100%' }}
          onPress={() => navigation.navigate('ScanQRcode')}
        />
      </View>
    ),
    headerLeft: () => (
      <View style={{ justifyContent: 'flex-end', flex: 1, flexDirection: 'column' }}>
        <Text style={{ fontSize: 34, fontWeight: 'bold', marginLeft: 16, marginBottom: 8 }}>
          บ้านของฉัน
        </Text>
      </View>
    ),
  });

  render() {
    const { navigate } = this.props.navigation;

    return (
      <View style={styles.background}>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <ScrollView ontentInsetAdjustmentBehavior="automatic" style={{ height: '100%' }}>
          <Card>
            <View style={{ flexDirection: 'row', width: '100%' }}>
              <Text style={{ marginBottom: 5, fontSize: 25, textAlign: 'left', width: '50%' }}>
                Sky's Home
              </Text>
              <Icons
                name="navigate-next"
                style={{ fontSize: 25, textAlign: 'right', width: '50%' }}
                onPress={() => navigate('CheckHistory')}
              />
            </View>
            <Text style={{ marginBottom: 5, fontSize: 25 }}>ยอดค่าไฟ</Text>
            <Text style={{ marginBottom: 5, fontSize: 30, fontWeight: 'bold' }}> 198.42</Text>
            <Text style={{ marginBottom: 5, fontSize: 20 }}>ครบกำหนดชำระ 10/11/2562</Text>
            <Button
              buttonStyle={{
                borderRadius: 0,
                marginLeft: 0,
                marginRight: 0,
                marginBottom: 0,
                borderRadius: 10,
                backgroundColor: '#ECECEC',
              }}
              title="จ่าย"
              type="clear"
              onPress={() => navigate('SelectPayment')}
            />
          </Card>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  background: {
    backgroundColor: '#fff',
  },
});
