import React, { Component } from 'react';
import { ScrollView, Text, View, StyleSheet } from 'react-native';
import { Card, ListItem, Button } from 'react-native-elements';
import Icons from 'react-native-vector-icons/MaterialIcons';

export default class SettingScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: '',
    headerStyle: { height: 90 },
    headerRight: () => (
      <View
        style={{
          justifyContent: 'flex-start',
          flex: 1,
          alignItems: 'flex-end',
        }}>
        <Icons
          style={{ marginRight: 8, marginTop: 8 }}
          name="notifications"
          size={30}
          color="#007aff"
        />
      </View>
    ),
    headerLeft: () => (
      <View style={{ justifyContent: 'flex-end', flex: 1, flexDirection: 'column' }}>
        <Text style={{ fontSize: 34, fontWeight: 'bold', marginLeft: 16, marginBottom: 8 }}>
          อื่นๆ
        </Text>
      </View>
    ),
  });

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={{ height: '100%', backgroundColor: '#fff' }}>
        <ScrollView ontentInsetAdjustmentBehavior="automatic">
          <ListItem
            bottomDivider
            title="ท้องฟ้า แจ่มใส"
            subtitle="id: 81990253xxxx                                                        เบอร์โทร: 0997891234"
            subtitleStyle={{ fontSize: 16 }}
            style={{
              paddingBottom: 10,
              paddingTop: 10,
              marginTop: 10,
              marginBottom: 10,
            }}
            titleStyle={{ fontSize: 30 }}
            rightIcon={{ name: 'navigate-next', color: '#ACADB0' }}
            leftIcon={{
              name: 'user',
              type: 'entypo',
              color: '#ACADB0',
              size: 50,
              marginRight: 10,
              marginLeft: 10,
            }}
          />

          <ListItem
            bottomDivider
            title="สถานที่ชำระ"
            containerStyle={{ margin: 0, marginBottom: 2 }}
            rightIcon={{ name: 'navigate-next', color: '#ACADB0' }}
          />
          <ListItem
            bottomDivider
            title="ประกาศไฟฟ้าดับ"
            containerStyle={{ margin: 0, marginBottom: 2 }}
            rightIcon={{ name: 'navigate-next', color: '#ACADB0' }}
          />
          <ListItem
            bottomDivider
            title="บริการขอใช้ไฟฟ้า(MEASY)"
            containerStyle={{ margin: 0, marginBottom: 2 }}
            rightIcon={{ name: 'navigate-next', color: '#ACADB0' }}
          />

          <ListItem
            bottomDivider
            title="คำนวณค่าไฟฟ้า"
            containerStyle={{ margin: 0, marginBottom: 2 }}
            rightIcon={{ name: 'navigate-next', color: '#ACADB0' }}
          />
          <ListItem
            bottomDivider
            title="เบอร์โทรฉุกเฉิน"
            containerStyle={{ margin: 0, marginBottom: 2 }}
            rightIcon={{ name: 'navigate-next', color: '#ACADB0' }}
          />
          <ListItem
            bottomDivider
            title="ตั้งค่าระบบ"
            containerStyle={{ margin: 0, marginBottom: 2 }}
            rightIcon={{ name: 'navigate-next', color: '#ACADB0' }}
          />
          <ListItem
            bottomDivider
            title="คู่มือการใช้งาน"
            containerStyle={{ margin: 0, marginBottom: 2 }}
            rightIcon={{ name: 'navigate-next', color: '#ACADB0' }}
          />
          <ListItem
            bottomDivider
            title="เกี่ยวกับ MEA Smart Life"
            containerStyle={{ margin: 0, marginBottom: 2 }}
            rightIcon={{ name: 'navigate-next', color: '#ACADB0' }}
          />
          <ListItem
            bottomDivider
            title="ออกจากระบบ"
            containerStyle={{ margin: 0, marginBottom: 2 }}
            onPress={() => navigate('First')}
            bottomDivider
            rightIcon={{ name: 'navigate-next', color: '#ACADB0' }}
          />
          <View style={{ marginBottom: 20 }}></View>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  textHeader: {
    fontWeight: 'bold',
    fontSize: 20,
    margin: 10,
  },
  listItemStyle: {
    margin: 5,
    borderBottomColor: '#E4933E',
  },
});
