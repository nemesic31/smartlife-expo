import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { Input, Button } from 'react-native-elements';
import { KeyboardAvoidingView } from 'react-native';

export default class FillmeterScreen extends Component {
  static navigationOptions = {
    title: 'ลงทะเบียนมิเตอร์ไฟฟ้า',
  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <KeyboardAvoidingView behavior="height" enabled keyboardVerticalOffset={250}>
        <View style={styles.background}>
          <Text style={styles.textHeader}>กรอกข้อมูลมิเตอร์ไฟฟ้า </Text>
          <Text style={styles.textHeader2}>
            กรอกบัญชีแสดงสัญญา 9 หลักและรหัสเครื่องวัด 8 หลัก ที่แสดงบนใบแจ้งค่าไฟฟ้าของคุณ
          </Text>
          <Input
            placeholder="บัญชีแสดงสัญญาเลที่"
            placeholderTextColor="gray"
            errorStyle={{ color: 'red' }}
            errorMessage="กรุณากรอกข้อมูลให้ถูกต้อง"
            containerStyle={{ marginBottom: 10, marginLeft: 10 }}
            keyboardType="numeric"
          />
          <Input
            placeholder="รหัสเครื่องวัดไฟฟ้า"
            placeholderTextColor="gray"
            errorStyle={{ color: 'red' }}
            errorMessage="กรุณากรอกข้อมูลให้ถูกต้อง"
            containerStyle={{ marginBottom: 30, marginLeft: 10 }}
            keyboardType="numeric"
          />
          <View style={{ justifyContent: 'flex-end', height: '30%' }}>
            <Button
              title="ต่อไป"
              type="clear"
              buttonStyle={{
                backgroundColor: '#ECECEC',
                borderRadius: 5,
                marginLeft: 10,
                marginRight: 10,
                marginBottom: 5,
              }}
              onPress={() => navigate('Namemeter')}
            />
            <Button
              title="ลงทะเบียนด้วยการสแกน QR Code"
              type="clear"
              buttonStyle={{
                marginLeft: 10,
                marginRight: 10,
              }}
              onPress={() => navigate('ScanQRcode')}
            />
          </View>
        </View>
      </KeyboardAvoidingView>
    );
  }
}
const styles = StyleSheet.create({
  background: {
    backgroundColor: '#fff',
    height: '100%',

    width: '100%',
  },
  textHeader: {
    fontSize: 35,
    fontWeight: 'bold',
    marginBottom: 10,
    marginTop: 20,
    marginLeft: 20,
  },
  textHeader2: {
    fontSize: 15,
    marginBottom: 20,
    marginLeft: 20,
  },
});
