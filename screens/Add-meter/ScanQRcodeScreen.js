import React, { Component } from 'react';

import {
  StyleSheet,
  View,
  Text,
  Platform,
  TouchableOpacity,
  Linking,
  PermissionsAndroid,
  StatusBar,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import { Button } from 'react-native-elements';
import Icons from 'react-native-vector-icons/Ionicons';

import { CameraKitCameraScreen } from 'react-native-camera-kit';

export default class ScanQRcodeScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'ลงทะเบียนมิเตอร์ไฟฟ้า',
    headerLeft: () => (
      <Icons
        style={{ marginLeft: 8 }}
        name={Platform.OS === 'ios' ? 'ios-arrow-back' : 'md-arrow-back'}
        size={30}
        color={Platform.OS === 'ios' ? '#007aff' : '#000'}
        onPress={() => navigation.navigate('Home')}
      />
    ),
  });
  constructor() {
    super();
    this.state = {
      QR_Code_Value: '',
      Start_Scanner: false,
    };
  }

  componentDidMount() {
    this.open_QR_Code_Scanner();
  }

  openLink_in_browser = () => {
    // Linking.openURL(this.state.QR_Code_Value);
  };

  onQR_Code_Scan_Done = async (QR_Code) => {
    await AsyncStorage.setItem('@QR_Code:key', QR_Code);
    this.props.navigation.navigate('Namemeter', { QR_Code: QR_Code });

    // this.setState({ Start_Scanner: false });
  };

  open_QR_Code_Scanner = () => {
    var that = this;
    if (Platform.OS === 'android') {
      async function requestCameraPermission() {
        try {
          const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CAMERA, {
            title: 'Camera App Permission',
            message: 'Camera App needs access to your camera ',
          });
          if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            that.setState({ QR_Code_Value: '' });
            // that.setState({ Start_Scanner: true });
          } else {
            alert('CAMERA permission denied');
          }
        } catch (err) {
          alert('Camera permission err', err);
          console.warn(err);
        }
      }
      requestCameraPermission();
    }
  };

  render() {
    const { navigate } = this.props.navigation;

    return (
      <View style={styles.MainContainer}>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <View>
          <Text style={styles.textHeader}>QR code</Text>
          <Text style={styles.textHeader2}>สแกน QR code ที่แสดงอยู่บนใบแจ้งค่าไฟฟ้าของคุณ</Text>
          <View style={Platform.OS === 'ios' && { height: '60%', justifyContent: 'center' }}>
            <CameraKitCameraScreen
              style={{ height: '100%' }}
              showFrame={true}
              scanBarcode={true}
              laserColor={'#FF3D00'}
              frameColor={'#00C853'}
              colorForScannerFrame={'black'}
              onReadCode={(event) => this.onQR_Code_Scan_Done(event.nativeEvent.codeStringValue)}
            />
          </View>
        </View>

        <View style={{ marginBottom: 25, justifyContent: 'center' }}>
          <View style={Platform.OS !== 'ios' && { width: 400, alignSelf: 'center' }}>
            <Button
              title="ลงทะเบียนด้วยการกรอก"
              type={Platform.OS === 'ios' ? 'clear' : 'solid'}
              onPress={() => navigate('Fillmeter')}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    backgroundColor: '#fff',
    flex: 2,
    flexDirection: 'column',
    justifyContent: 'space-between',

    paddingTop: Platform.OS === 'ios' ? 20 : 0,
  },
  QR_text: {
    color: '#000',
    fontSize: 19,
    padding: 8,
    marginTop: 12,
  },
  button: {
    backgroundColor: '#2979FF',
    alignItems: 'center',
    padding: 12,
    width: 300,
    marginTop: 14,
  },
  background: {
    backgroundColor: '#fff',
    height: '100%',

    width: '100%',
  },
  textHeader: {
    fontSize: 35,
    fontWeight: 'bold',
    marginBottom: 10,
    marginTop: 2,
    marginLeft: 20,
  },
  textHeader2: {
    fontSize: 15,
    marginBottom: 10,
    marginLeft: 20,
  },
});
