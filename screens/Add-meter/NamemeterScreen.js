import React, { Component } from 'react';
import { Text, View, StyleSheet, ScrollView } from 'react-native';
import { Button, Input } from 'react-native-elements';
import AsyncStorage from '@react-native-community/async-storage';
import ToggleSwitch from 'toggle-switch-react-native';

export default class NamemeterScreen extends Component {
  static navigationOptions = {
    title: 'ลงทะเบียนมิเตอร์ไฟฟ้า',
  };

  constructor() {
    super();
    this.state = {
      toggle: false,
      qrcode: '',
    };
  }

  componentDidMount() {
    this.getData();
  }

  check = (event) => {
    console.log(event);
  };

  getData = async () => {
    const qrcode = await AsyncStorage.getItem('@QR_Code:key');
    console.log({ qrcode });
    await this.setState({ qrcode });
  };

  render() {
    const { navigate } = this.props.navigation;
    const { qrcode } = this.state;
    return (
      <View style={styles.background}>
        <ScrollView ontentInsetAdjustmentBehavior="automatic" style={{ height: '100%' }}>
          <View style={{ marginBottom: '50%' }}>
            <Text style={styles.textHeader}> ข้อมูลมิเตอร์ไฟฟ้า </Text>
            <Text style={styles.textHeader2}> บัญชีแสดงสัญญาเลขที่ </Text>
            <Input
              placeholder="บัญชีแสดงสัญญาเลขที่"
              placeholderTextColor="gray"
              errorStyle={{ color: 'red' }}
              containerStyle={{ marginBottom: 10, marginLeft: 10 }}>
              {qrcode && qrcode}
            </Input>
            <Text style={styles.textHeader2}> รหัสเครื่องวัดไฟฟ้า </Text>
            <Input
              placeholder="บัญชีแสดงสัญญาเลขที่"
              placeholderTextColor="gray"
              errorStyle={{ color: 'red' }}
              containerStyle={{ marginBottom: 30, marginLeft: 10 }}>
              {qrcode && qrcode}
            </Input>

            <Input
              placeholder="ชื่อเจ้าของมิเตอร์"
              placeholderTextColor="gray"
              errorStyle={{ color: 'red' }}
              containerStyle={{ marginBottom: 20, marginLeft: 10 }}
            />
            <Input
              placeholder="สถานที่ตั้ง"
              placeholderTextColor="gray"
              errorStyle={{ color: 'red' }}
              containerStyle={{ marginBottom: 20, marginLeft: 10 }}
            />

            <Text style={styles.textHeader}>ตั้งชื่อมิเตอร์ไฟฟ้าของคุณ</Text>

            <Input
              placeholder="ชื่อมิเตอร์ไฟฟ้า"
              placeholderTextColor="gray"
              errorStyle={{ color: 'red' }}
              containerStyle={{ marginBottom: 20, marginLeft: 10 }}
            />
            <ToggleSwitch
              isOn={this.state.toggle}
              onColor="green"
              offColor="gray"
              label="ที่บ้านมีผู้ป่วยติดเตียงใช่ไหม"
              labelStyle={{ marginLeft: 25, fontSize: 17, marginRight: 60 }}
              size="large"
              onToggle={(isOn) =>
                this.state.toggle
                  ? this.setState({ toggle: false })
                  : this.setState({ toggle: true })
              }
            />
            <View style={{ backgroundColor: '#efeff4', marginTop: 10 }}>
              <Text
                style={{
                  marginLeft: 20,
                  marginTop: 10,
                  marginBottom: 10,
                  color: '#8a8a8f',
                  fontWeight: '500',
                  textAlign: 'left',
                }}>
                กรณีมีผู้ป่วยติดเตียง ที่ต้องึ่งอุปกรณ์ไฟฟ้าช่วยชีวิต{'\n'}
                กรุณาเลือกเพื่อรับสิทธ์การช่วยเหลือฉุกเฉิน ข้อมูลเพิ่มเติม
              </Text>
            </View>
            <Button
              title="ตกลง"
              type="clear"
              style={{ marginTop: 20 }}
              onPress={() => navigate('Home')}
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  background: {
    backgroundColor: '#fff',
    height: '100%',

    width: '100%',
  },
  textHeader: {
    fontSize: 30,
    fontWeight: 'bold',
    marginBottom: 20,
    marginTop: 20,
    marginLeft: 20,
  },
  textHeader2: {
    fontSize: 15,
    marginBottom: 5,
    marginLeft: 20,
  },
});
