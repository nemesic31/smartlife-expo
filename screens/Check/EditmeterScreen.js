import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { Button, Input } from 'react-native-elements';
import ToggleSwitch from 'toggle-switch-react-native';

export default class EditmeterScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'แก้ไขข้อมูลมิเตอร์ไฟฟ้า',
    headerRight: () => (
      <Button
        buttonStyle={{
          marginRight: 8,
        }}
        title="เสร็จสิ้น"
        titleStyle={{ color: '#395DE2' }}
        type="clear"
        onPress={() => navigation.navigate('CheckHistory')}
      />
    ),
  });

  constructor() {
    super();
    this.state = {
      toggle: false,
    };
  }

  check = (event) => {
    console.log(event);
  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.background}>
        <View style={{ marginTop: 8 }}>
          <Text style={styles.textHeader}> ข้อมูลมิเตอร์ไฟฟ้า </Text>
          <Input placeholder="บัญชีแสดงสัญญาเลขที่" containerStyle={styles.inputStyle} />
          <Input placeholder="รหัสเครื่องวัดไฟฟ้า" containerStyle={styles.inputStyle} />
          <Input placeholder="ชื่อเจ้าของมิเอตร์ไฟฟ้า" containerStyle={styles.inputStyle} />
          <Input placeholder="สถานที่ตั้ง" containerStyle={styles.inputStyle} />
          <Input placeholder="ชื่อมิเตอร์ไฟฟ้า" containerStyle={styles.inputStyle} />
          <ToggleSwitch
            isOn={this.state.toggle}
            onColor="green"
            offColor="gray"
            label="ที่บ้านมีผู้ป่วยติดเตียงใช่ไหม"
            labelStyle={{ marginLeft: 25, fontSize: 17, marginRight: 60 }}
            size="large"
            onToggle={(isOn) =>
              this.state.toggle ? this.setState({ toggle: false }) : this.setState({ toggle: true })
            }
          />
          <View style={{ backgroundColor: '#efeff4', marginTop: 10 }}>
            <Text
              style={{
                marginLeft: 20,
                marginTop: 10,
                marginBottom: 10,
                color: '#8a8a8f',
                fontWeight: '500',
                textAlign: 'left',
              }}>
              กรณีมีผู้ป่วยติดเตียง ที่ต้องึ่งอุปกรณ์ไฟฟ้าช่วยชีวิต{'\n'}
              กรุณาเลือกเพื่อรับสิทธ์การช่วยเหลือฉุกเฉิน ข้อมูลเพิ่มเติม
            </Text>
          </View>
        </View>

        <View>
          <Button
            buttonStyle={{
              marginRight: 10,
              marginLeft: 5,
              marginBottom: 10,
              borderRadius: 10,

              width: 400,
            }}
            title="ลบมิเตอร์ไฟฟ้า"
            titleStyle={{ color: '#395DE2' }}
            type="clear"
            onPress={() => navigate('Home')}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  background: {
    backgroundColor: '#ffffff',
    // height: '100%',
    flex: 2,
    flexDirection: 'column',
    justifyContent: 'space-between',
    // alignItems: 'center',
  },
  textHeader: {
    fontSize: 20,
    fontWeight: 'bold',
    marginLeft: 10,
    marginBottom: 20,
    marginTop: 8,
  },
  inputStyle: {
    marginBottom: 15,
  },
});
