import React, { Component } from 'react';
import { Text, View, StyleSheet, ScrollView, StatusBar } from 'react-native';
import { Button, ButtonGroup } from 'react-native-elements';
import { BarChart } from 'react-native-chart-kit';
import { Table, Row, Rows } from 'react-native-table-component';
import { Dimensions } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

export default class CheckHistoryScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: "รายละเอียด Sky's Home",
    headerRight: () => (
      <Button
        buttonStyle={{
          marginRight: 16,
        }}
        title="แก้ไข"
        titleStyle={{ color: '#395DE2' }}
        type="clear"
        onPress={() => navigation.navigate('Editmeter')}
      />
    ),
    headerLeft: () => (
      <View style={{ justifyContent: 'flex-end', flex: 1, flexDirection: 'column' }}>
        <Icon
          name="ios-arrow-back"
          style={{ fontSize: 20, color: '#007AFF', marginLeft: 10, marginBottom: 10 }}
          onPress={() => navigation.navigate('Home')}
        />
      </View>
    ),
  });

  constructor() {
    super();
    this.state = {
      selectedIndex: 0,
      tableHeadBath: ['วันที่\nจดเลขอ่าน', 'ค่าไฟฟ้า\n(บาท)', 'ส่วนต่าง\nระหว่างเดือน'],
      tableHeadKWH: ['วันที่\nจดเลขอ่าน', 'ค่าไฟฟ้า\n(kWh)', 'ส่วนต่าง\nระหว่างเดือน'],
      tableDataBath: [
        ['24/10/2562', '210.5', '+65.3'],
        ['24/09/2562', '145.2', '-195'],
        ['24/08/2562', '340.2', '-260.34'],
        ['24/07/2562', '600.34', '+511.14'],
        ['24/06/2562', '89.2', '+58.7'],
        ['24/05/2562', '30.5', '-'],
      ],
      tableDataKWH: [
        ['24/10/2562', '30', '+10'],
        ['24/09/2562', '20', '-20'],
        ['24/08/2562', '40', '-10'],
        ['24/07/2562', '50', '+40'],
        ['24/06/2562', '10', '+5'],
        ['24/05/2562', '5', '-'],
      ],
    };
  }

  updateIndex = (selectedIndex) => {
    this.setState({ selectedIndex });
  };

  GraphBath() {
    const screenWidth = Dimensions.get('window').width;
    const barData = {
      labels: ['พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.'],
      datasets: [
        {
          data: [210.5, 145.2, 340.2, 600.34, 89.2, 30.5],
        },
      ],
    };
    return (
      <View>
        <BarChart
          data={barData}
          width={screenWidth}
          height={220}
          yAxisLabel={'฿'}
          chartConfig={chartConfig}
        />
        <Table
          containerStyle={{}}
          borderStyle={{ borderWidth: 1, borderColor: '#DCDCDC' }}
          style={{ marginTop: 20, alignSelf: 'center' }}>
          <Row
            data={this.state.tableHeadBath}
            textStyle={{ fontSize: 20, textAlign: 'center' }}
            widthArr={[150, 75, 150]}
          />
          <Rows
            data={this.state.tableDataBath}
            textStyle={{ fontSize: 15, textAlign: 'center' }}
            widthArr={[150, 75, 150]}
            heightArr={[50, 50, 50, 50, 50, 50]}
          />
        </Table>
      </View>
    );
  }

  GraphKWH() {
    const screenWidth = Dimensions.get('window').width;
    const barData = {
      labels: ['พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.'],
      datasets: [
        {
          data: [30, 20, 40, 50, 10, 5],
        },
      ],
    };
    return (
      <View>
        <BarChart
          data={barData}
          width={screenWidth}
          height={220}
          yAxisLabel={''}
          chartConfig={chartConfig}
        />
        <Table
          borderStyle={{ borderWidth: 1, borderColor: '#DCDCDC', borderRadius: 1 }}
          style={{ marginTop: 20, alignSelf: 'center' }}>
          <Row
            data={this.state.tableHeadKWH}
            textStyle={{ fontSize: 20, textAlign: 'center' }}
            widthArr={[150, 75, 150]}
          />
          <Rows
            data={this.state.tableDataKWH}
            textStyle={{ fontSize: 15, textAlign: 'center' }}
            widthArr={[150, 75, 150]}
            heightArr={[50, 50, 50, 50, 50, 50]}
          />
        </Table>
      </View>
    );
  }

  render() {
    const buttons = ['หน่วยเงิน(บาท)', 'หน่วยไฟฟ้า(kWh)'];
    const { selectedIndex } = this.state;
    const { navigate } = this.props.navigation;

    return (
      <View style={styles.background}>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <Text style={styles.textHeader}>ประวัติการใช้ไฟฟ้า</Text>
        <ButtonGroup
          onPress={this.updateIndex}
          selectedIndex={selectedIndex}
          buttons={buttons}
          containerStyle={{ marginBottom: 40 }}
          selectedButtonStyle={{ backgroundColor: '#D5D5D5' }}
        />
        <View style={{ justifyContent: 'center', width: '100%' }}>
          <ScrollView ontentInsetAdjustmentBehavior="automatic" style={{ height: '100%' }}>
            <View style={{ marginBottom: '50%' }}>
              {this.state.selectedIndex === 0 ? this.GraphBath() : this.GraphKWH()}
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  background: {
    backgroundColor: '#ffffff',
    height: '100%',
    flex: 1,
  },
  textHeader: {
    fontSize: 20,
    fontWeight: 'bold',
    marginLeft: 10,
    marginBottom: 20,
    marginTop: 15,
  },
});
const chartConfig = {
  backgroundGradientFrom: '#fff',
  backgroundGradientTo: '#fff',
  color: (opacity = 1) => `rgba(72, 73, 76 , ${opacity})`,
  strokeWidth: 2, // optional, default 3
};
