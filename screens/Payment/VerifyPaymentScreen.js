import React, { Component } from 'react';
import { Text, View, StyleSheet, ScrollView } from 'react-native';
import { Input, Button, Divider } from 'react-native-elements';

import ToggleSwitch from 'toggle-switch-react-native';

export default class VerifyPaymentScreen extends Component {
  static navigationOptions = {
    title: 'จ่าย',
  };
  render() {
    const { navigate } = this.props.navigation;
    return (
      <ScrollView ontentInsetAdjustmentBehavior="automatic" style={{ backgroundColor: '#fff' }}>
        <View style={styles.background}>
          <View>
            <Text
              style={{
                fontSize: 18,
                marginBottom: 16,
                marginLeft: 20,
                marginTop: 15,
                fontWeight: 'bold',
              }}>
              Sky's Home
            </Text>

            <Text style={{ fontSize: 14, marginBottom: 5, marginLeft: 20, color: '#4d4d4d' }}>
              บัญชีแสดงสัญญา 213456789
            </Text>
            <Text style={{ fontSize: 14, marginBottom: 14, marginLeft: 20, color: '#999999' }}>
              220/123 คอนโดTC Green ถนน พระราม9 กรุงเทพ
            </Text>
            <Text style={{ fontSize: 22, marginBottom: 5, marginLeft: 15 }}> ยอดชำระทั้งหมด </Text>
            <Text style={{ fontSize: 34, marginBottom: 5, marginLeft: 20, fontWeight: 'bold' }}>
              204.42 บาท
            </Text>
            <Divider style={styles.dividerStyle} />
            <Text style={{ fontSize: 22, marginBottom: 16, marginLeft: 20, fontWeight: 'bold' }}>
              กรอกข้อมูลบัตรเครดิตเพื่อชำระ
            </Text>
            <View style={{ alignItems: 'center', marginBottom: 20 }}>
              <Input
                placeholder="หมายเลขบัตร"
                containerStyle={{ marginBottom: 12 }}
                keyboardType="number-pad"
                placeholderTextColor="gray"
              />
              <Input
                placeholder="ชื่อผู้ถือบัตร"
                containerStyle={{ marginBottom: 12 }}
                placeholderTextColor="gray"
              />
              <Input
                placeholder="วันหมดอายุ"
                containerStyle={{ marginBottom: 12 }}
                placeholderTextColor="gray"
              />
              <Input
                placeholder="เลขสามตัวหลังบัตร"
                containerStyle={{ marginBottom: 12 }}
                keyboardType="number-pad"
                placeholderTextColor="gray"
              />
            </View>
          </View>
          <View style={{ justifyContent: 'flex-end' }}>
            <Button
              title="ยืนยันการจ่าย"
              type="clear"
              buttonStyle={{
                backgroundColor: '#E5E5E5',
                borderRadius: 5,
                marginLeft: 15,
                marginRight: 15,
              }}
              onPress={() => navigate('BillPayment')}
            />
          </View>
        </View>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  background: {
    backgroundColor: '#fff',
    flex: 2,
    justifyContent: 'space-between',
    flexDirection: 'column',
  },
  textHeader: {
    fontSize: 35,
    fontWeight: 'bold',
    marginBottom: 10,
    marginTop: 20,
    marginLeft: 20,
  },
  textHeader2: {
    fontSize: 15,
    marginBottom: 10,
    marginLeft: 20,
  },
  wrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
  },
  dividerStyle: {
    marginTop: 15,
    marginBottom: 15,
    width: '90%',
    alignSelf: 'center',
    backgroundColor: '#000',
  },
});
