import React, { Component } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, StatusBar, ScrollView } from 'react-native';
import { Card, Button, Divider } from 'react-native-elements';

import Icons from 'react-native-vector-icons/FontAwesome';
import IconsIonic from 'react-native-vector-icons/Ionicons';

import Swiper from 'react-native-swiper';

export default class SelectPaymentScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: 'จ่าย',
    headerRight: () => (
      <Button
        title="จ่ายที่เคาท์เตอร์"
        type="clear"
        containerStyle={{ marginRight: 8 }}
        onPress={() => navigation.navigate('QRcodemeter')}
      />
    ),
    headerLeft: () => (
      <IconsIonic
        style={{ marginLeft: 8 }}
        name={Platform.OS === 'ios' ? 'ios-arrow-back' : 'md-arrow-back'}
        size={30}
        color={Platform.OS === 'ios' ? '#007aff' : '#000'}
        onPress={() => navigation.navigate('Home')}
      />
    ),
  });

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.background}>
        <StatusBar backgroundColor="white" barStyle="dark-content" />
        <ScrollView ontentInsetAdjustmentBehavior="automatic">
          <View>
            <Text
              style={{
                fontSize: 18,
                marginBottom: 16,
                marginLeft: 20,
                marginTop: 15,
                fontWeight: 'bold',
              }}>
              Sky's Home
            </Text>

            <Text style={{ fontSize: 14, marginBottom: 5, marginLeft: 20, color: '#4d4d4d' }}>
              บัญชีแสดงสัญญา 213456789
            </Text>
            <Text style={{ fontSize: 14, marginBottom: 14, marginLeft: 20, color: '#999999' }}>
              220/123 คอนโดTC Green ถนน พระราม9 กรุงเทพ
            </Text>
            <Text style={{ fontSize: 22, marginBottom: 5, marginLeft: 20 }}>ยอดค่าไฟฟ้า</Text>
            <Text style={{ fontSize: 34, marginBottom: 5, marginLeft: 20, fontWeight: 'bold' }}>
              198.42 บาท
            </Text>
            <Text
              style={{
                fontSize: 18,
                marginBottom: 14,
                marginLeft: 20,
                fontWeight: 'bold',
                color: '#4d4d4d',
              }}>
              ครบกำหนดชำระ 10/11/2562
            </Text>
            <Text style={{ fontSize: 22, marginBottom: 16, marginLeft: 20, fontWeight: 'bold' }}>
              เลือกช่องทางการจ่าย
            </Text>
            <Text style={{ fontSize: 14, marginBottom: -10, marginLeft: 20, color: '#999999' }}>
              ช่องทางที่ใช้จ่ายครั้งล่าสุด
            </Text>
            <TouchableOpacity onPress={() => navigate('PaymentCreditForm')}>
              <Card
                containerStyle={{
                  backgroundColor: '#efeff4',
                  borderRadius: 10,
                  justifyContent: 'center',
                  borderColor: '#efeff4',
                }}>
                <Text style={{ fontSize: 30, color: '#6b6b6d', fontWeight: 'bold' }}>
                  ค่าธรรมเนียม 5 บาท
                </Text>
                <View
                  style={{
                    flexDirection: 'row',
                    width: '100%',
                    flex: 2,
                    justifyContent: 'space-between',
                  }}>
                  <View>
                    <Text style={{ fontSize: 15, marginBottom: 5 }}>
                      จ่ายด้วยบัตรเครดิตทุกธนาคาร
                    </Text>
                  </View>
                  <View s>
                    <Icons name="cc-mastercard" style={{ fontSize: 25, textAlign: 'right' }} />
                  </View>
                </View>
              </Card>
            </TouchableOpacity>
          </View>
          <Divider style={styles.dividerStyle} />
          <View>
            <View
              style={{
                flexDirection: 'row',
                width: '100%',
                flex: 2,
                justifyContent: 'space-between',
                marginBottom: 14,
              }}>
              <View>
                <Text style={{ fontSize: 18, fontWeight: 'bold', marginLeft: 20 }}>
                  ช่องทางการจ่าย
                </Text>
              </View>
              <View>
                <Button
                  title="ดูทั้งหมด"
                  type="clear"
                  containerStyle={{ alignItems: 'flex-end', marginRight: 10 }}
                />
              </View>
            </View>

            <Swiper style={styles.wrapper} containerStyle={{ height: 175 }}>
              <TouchableOpacity onPress={() => navigate('PaymentCreditForm')}>
                <Card
                  containerStyle={{
                    backgroundColor: '#efeff4',
                    borderRadius: 10,
                    borderColor: '#efeff4',
                    justifyContent: 'center',
                  }}>
                  <Text style={{ fontSize: 30, color: '#6b6b6d', fontWeight: 'bold' }}>
                    ค่าธรรมเนียม 6 บาท
                  </Text>
                  <Text style={{ fontSize: 15, marginBottom: 5 }}>จ่ายด้วยบัตรเครดิตทุกธนาคาร</Text>
                </Card>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => navigate('PaymentCreditForm')}>
                <Card
                  containerStyle={{
                    backgroundColor: '#efeff4',
                    borderRadius: 10,
                    borderColor: '#efeff4',
                    justifyContent: 'center',
                  }}>
                  <Text style={{ fontSize: 30, color: '#6b6b6d', fontWeight: 'bold' }}>
                    ค่าธรรมเนียม 10 บาท
                  </Text>
                  <Text style={{ fontSize: 15, marginBottom: 5 }}>
                    จ่ายด้วย Internet Banking ทุกธนาคาร
                  </Text>
                </Card>
              </TouchableOpacity>
            </Swiper>
          </View>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  background: {
    backgroundColor: '#fff',
    flex: 1,
    flexDirection: 'column',
    width: '100%',
  },
  dividerStyle: {
    marginTop: 20,
    marginBottom: 8,
    width: '90%',
    alignSelf: 'center',
    backgroundColor: '#000',
  },
  wrapper: {
    alignItems: 'center',
  },
});
