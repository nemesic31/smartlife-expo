import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { Button } from 'react-native-elements';
import Icons from 'react-native-vector-icons/SimpleLineIcons';
import { Table, Rows } from 'react-native-table-component';

export default class BillPaymentScreen extends Component {
  static navigationOptions = {
    title: 'ดำเนินการจ่าย',
  };
  constructor(props) {
    super(props);
    this.state = {
      tableData: [
        ['เลขที่ใบแจ้งค่าไฟฟ้า', '576868924'],
        ['บัญชีแสดงสัญญา', '213465879'],
        ['ชื่อลูกค้า', 'ท้องฟ้า แจ่มใส'],
        ['ค่าไฟฟ้า', '198.42 บาท'],
        ['ค่าธรรมเนียม', '6.00 บาท'],
        ['วันที่ชำระ', '10/01/2562 15:30:25'],
        ['ยอดชำระทั้งหมด', '204.42 บาท'],
      ],
    };
  }
  render() {
    const state = this.state;
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <View style={{ alignItems: 'center' }}>
          <Icons name="check" style={{ fontSize: 100, marginTop: 20 }} />
          <Text style={styles.textHeader}>ดำเนินการสำเร็จ</Text>
        </View>
        <Table
          borderStyle={{ borderWidth: 2, borderColor: '#fff' }}
          style={{ marginLeft: 20, marginRight: 20 }}>
          <Rows data={state.tableData} textStyle={styles.text} />
        </Table>
        <Button
          title="กลับสู่บ้านของฉัน"
          type="clear"
          buttonStyle={{
            backgroundColor: '#E5E5E5',
            borderRadius: 5,
            marginLeft: 15,
            marginRight: 15,
            marginTop: 5,
            marginBottom: 20,
          }}
          onPress={() => navigate('Home')}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  background: {
    backgroundColor: '#fff',
    height: '100%',

    width: '100%',
  },
  textHeader: {
    fontSize: 35,
    fontWeight: 'bold',
    marginBottom: 20,
    marginTop: 20,
  },
  textHeader2: {
    fontSize: 15,
    marginBottom: 10,
    marginLeft: 20,
  },
  wrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
  },
  container: {
    flex: 1,
    padding: 16,
    paddingTop: 30,
    backgroundColor: '#fff',
  },
  head: {
    height: 40,
    backgroundColor: '#f1f8ff',
  },
  text: {
    margin: 6,
    fontSize: 15,
  },
});
