import React, { Component } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import { Text, View, StyleSheet, Image } from 'react-native';
import { Card, ButtonGroup } from 'react-native-elements';
import QRCode from 'react-native-qrcode-svg';
import Barcode from 'react-native-barcode-builder';

export default class QRcodemeterScreen extends Component {
  static navigationOptions = {
    title: 'จ่ายที่เคาท์เตอร์',
  };
  constructor() {
    super();
    this.state = {
      selectedIndex: 0,
      getqrcode: '1234',
    };
  }

  componentDidMount() {
    this.getData();
  }

  getData = async () => {
    const qrcode = await AsyncStorage.getItem('@QR_Code:key');
    this.setState({ getqrcode: qrcode });
  };

  updateIndex = (selectedIndex) => {
    this.setState({ selectedIndex });
  };

  getQRcode() {
    const { getqrcode } = this.state;
    return <QRCode value={getqrcode} size={200} />;
  }

  Barcode() {
    return (
      <View>
        <Barcode value={this.state.getqrcode} format="CODE128" />
        <Text style={{ textAlign: 'center', fontWeight: 'bold' }}>{this.state.getqrcode}</Text>
      </View>
    );
  }

  render() {
    const buttons = ['QR Code', 'Barcode'];

    const { selectedIndex } = this.state;
    const { navigate } = this.props.navigation;

    return (
      <View style={styles.background}>
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'flex-end',
            height: '80%',
            width: '100%',
          }}>
          <View
            style={{
              width: '100%',
              height: '100%',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            {this.state.selectedIndex === 0 ? this.getQRcode() : this.Barcode()}
          </View>

          <Text style={styles.textHeader2}>
            ชำระที่การไฟฟ้านครหลวง,เทสโก้โลตัส,แฟมมิลี่มาร์ท,ท็อปส์และบิ๊กซี
          </Text>
          <ButtonGroup
            onPress={this.updateIndex}
            selectedIndex={selectedIndex}
            buttons={buttons}
            containerStyle={{ height: 50 }}
            selectedButtonStyle={{ backgroundColor: '#D5D5D5' }}
          />
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  background: {
    backgroundColor: '#fff',
    height: '100%',

    width: '100%',
  },

  textHeader2: {
    fontSize: 15,
    marginBottom: 10,
    marginLeft: 20,
    fontWeight: '100',
    marginRight: 20,
  },
});
