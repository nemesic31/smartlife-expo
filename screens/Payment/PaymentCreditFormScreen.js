import React, { Component } from 'react';
import { Text, View, StyleSheet, KeyboardAvoidingView, ScrollView } from 'react-native';
import { Input, Button, Divider } from 'react-native-elements';

import ToggleSwitch from 'toggle-switch-react-native';

export default class PaymentCreditFormScreen extends Component {
  static navigationOptions = {
    title: 'จ่าย',
  };
  constructor() {
    super();
    this.state = {
      toggle: false,
    };
  }
  check = (event) => {
    console.log(event);
  };

  render() {
    const { navigate } = this.props.navigation;

    return (
      <View style={styles.background}>
        <KeyboardAvoidingView
          behavior={Platform.OS === 'ios' ? 'padding' : null}
          enabled={Platform.OS === 'ios' ? true : false}
          keyboardVerticalOffset={Platform.OS === 'ios' ? 60 : 0}>
          <ScrollView ontentInsetAdjustmentBehavior="automatic">
            <View>
              <Text
                style={{
                  fontSize: 18,
                  marginBottom: 16,
                  marginLeft: 20,
                  marginTop: 15,
                  fontWeight: 'bold',
                }}>
                Sky's Home
              </Text>
              <Text style={{ fontSize: 14, marginBottom: 5, marginLeft: 20, color: '#4d4d4d' }}>
                บัญชีแสดงสัญญา 213456789
              </Text>
              <Text style={{ fontSize: 14, marginBottom: 14, marginLeft: 20, color: '#999999' }}>
                220/123 คอนโดTC Green ถนน พระราม9 กรุงเทพ
              </Text>
              <Text style={{ fontSize: 22, marginBottom: 5, marginLeft: 15 }}> ค่าธรรมเนียม </Text>
              <Text style={{ fontSize: 22, marginBottom: 5, marginLeft: 20, fontWeight: 'bold' }}>
                6.00 บาท
              </Text>
              <Text style={{ fontSize: 22, marginBottom: 5, marginLeft: 15 }}>
                {' '}
                ยอดชำระทั้งหมด{' '}
              </Text>
              <Text style={{ fontSize: 34, marginBottom: 5, marginLeft: 20, fontWeight: 'bold' }}>
                204.42 บาท
              </Text>
            </View>
            <Divider style={styles.dividerStyle} />
            <View>
              <Text style={{ fontSize: 22, marginBottom: 16, marginLeft: 20, fontWeight: 'bold' }}>
                กรอกอีเมล์เพื่อรับใบเสร็จค่าไฟฟ้า
              </Text>
              <Input
                placeholder="อีเมล์"
                placeholderTextColor="gray"
                containerStyle={{ marginLeft: 15, marginBottom: 20 }}
              />
              <ToggleSwitch
                isOn={this.state.toggle}
                onColor="green"
                offColor="gray"
                label="จดจำอีเมล์"
                labelStyle={{ marginLeft: 25, fontSize: 17, marginRight: 170 }}
                size="large"
                onToggle={(isOn) =>
                  this.state.toggle
                    ? this.setState({ toggle: false })
                    : this.setState({ toggle: true })
                }
              />
            </View>
            <View style={{ marginTop: 40 }}>
              <Button
                title="ต่อไป"
                type="clear"
                buttonStyle={{
                  backgroundColor: '#E5E5E5',
                  borderRadius: 5,
                  marginLeft: 15,
                  marginRight: 15,
                }}
                onPress={() => navigate('VerifyPayment')}
              />
            </View>
          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  background: {
    backgroundColor: '#fff',
    flex: 1,
    flexDirection: 'column',
  },
  dividerStyle: {
    marginTop: 20,
    marginBottom: 20,
    width: '90%',
    alignSelf: 'center',
    backgroundColor: '#000',
  },
  textHeader: {
    fontSize: 35,
    fontWeight: 'bold',
    marginBottom: 10,
    marginTop: 20,
    marginLeft: 20,
  },
  textHeader2: {
    fontSize: 15,
    marginBottom: 10,
    marginLeft: 20,
  },
  wrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
  },
});
