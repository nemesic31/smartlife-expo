import { createStackNavigator } from 'react-navigation-stack';
import SelectAlretPublicScreen from '../screens/InformPublic/InformHome/SelectAlretPublicScreen';
import MapPublicScreen from '../screens/InformPublic/InformHome/MapPublicScreen';
import CompleteProcessPublicScreen from '../screens/InformPublic/InformHome/CompleteProcessPublicScreen';
import MainTabNavigator from './MainTabNavigator';

const InformPublicStack = createStackNavigator({
  App: { screen: MainTabNavigator, navigationOptions: { headerShown: false } },
  SelectAlretPublic: SelectAlretPublicScreen,
  MapPublic: MapPublicScreen,
  CompleteProcessPublic: CompleteProcessPublicScreen,
});

export default InformPublicStack;
