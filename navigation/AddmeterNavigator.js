import { createStackNavigator } from 'react-navigation-stack';
import ScanQRcodeScreen from '../screens/Add-meter/ScanQRcodeScreen';
import FillmeterScreen from '../screens/Add-meter/FillmeterScreen';
import NamemeterScreen from '../screens/Add-meter/NamemeterScreen';
import MainTabNavigator from './MainTabNavigator';

const AddmeterStack = createStackNavigator({
  App: { screen: MainTabNavigator, navigationOptions: { headerShown: false } },
  ScanQRcode: ScanQRcodeScreen,
  Fillmeter: FillmeterScreen,
  Namemeter: NamemeterScreen,
});
export default AddmeterStack;
