import { createStackNavigator } from 'react-navigation-stack';
import SelectPaymentScreen from '../screens/Payment/SelectPaymentScreen';
import QRcodemeterScreen from '../screens/Payment/QRcodemeterScreen';
import PaymentCreditFormScreen from '../screens/Payment/PaymentCreditFormScreen';
import VerifyPaymentScreen from '../screens/Payment/VerifyPaymentScreen';
import BillPaymentScreen from '../screens/Payment/BillPaymentScreen';
import MainTabNavigator from './MainTabNavigator';

const PaymentStack = createStackNavigator({
  App: { screen: MainTabNavigator, navigationOptions: { headerShown: false } },
  SelectPayment: SelectPaymentScreen,
  QRcodemeter: QRcodemeterScreen,
  PaymentCreditForm: PaymentCreditFormScreen,
  VerifyPayment: VerifyPaymentScreen,
  BillPayment: BillPaymentScreen,
});
export default PaymentStack;
