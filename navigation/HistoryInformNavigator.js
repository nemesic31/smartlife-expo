import { createStackNavigator } from 'react-navigation-stack';
import HistoryAlertScreen from '../screens/HistoryInform/HistoryAlretScreen';
import DetailHistoryAlretScreen from '../screens/HistoryInform/DetailHistoryAlretScreen';
import MainTabNavigator from './MainTabNavigator';

const HistoryInformStack = createStackNavigator({
  App: { screen: MainTabNavigator, navigationOptions: { headerShown: false } },
  HistoryAlret: HistoryAlertScreen,
  DetailHistoryAlret: DetailHistoryAlretScreen,
});

export default HistoryInformStack;
