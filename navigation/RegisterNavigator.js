import { createStackNavigator } from 'react-navigation-stack';

import FirstScreen from '../screens/FirstScreen';
import ConditionScreen from '../screens/ConditionScreen';
import RegisterScreen from '../screens/RegisterScreen';
import VerifyScreen from '../screens/VerifyScreen';

const RegisterStack = createStackNavigator({
  First: FirstScreen,
  Condition: ConditionScreen,
  Register: RegisterScreen,
  Verify: VerifyScreen,
});

export default RegisterStack;
