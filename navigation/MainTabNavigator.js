import React from 'react';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';

import HomeScreen from '../screens/HomeScreen';
import NewsScreen from '../screens/NewsSceen';
import AlretScreen from '../screens/AlretScreen';
import SettingScreen from '../screens/SettingScreen';

const HomeStack = createStackNavigator({
  Home: HomeScreen,
});

HomeStack.navigationOptions = { tabBarLabel: 'บ้านของฉัน' };

const NewsStack = createStackNavigator({
  News: NewsScreen,
});

NewsStack.navigationOptions = { tabBarLabel: 'กฟน. วันนี้' };

const AlretStack = createStackNavigator({
  Alret: AlretScreen,
});

AlretStack.navigationOptions = { tabBarLabel: 'แจ้งเหตุ' };

const SettingStack = createStackNavigator({
  Setting: SettingScreen,
});

SettingStack.navigationOptions = { tabBarLabel: 'อื่นๆ' };

const tabNavigator = createBottomTabNavigator(
  {
    Home: HomeStack,
    Alret: AlretStack,
    News: NewsStack,
    Setting: SettingStack,
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let IconComponent = Icons;
        let iconName;
        if (routeName === 'Home') {
          iconName = `home-outline`;
          // Sometimes we want to add badges to some icons.
          // You can check the implementation below.
          // IconComponent = HomeIconWithBadge;
        } else if (routeName === 'News') {
          iconName = `newspaper`;
        } else if (routeName === 'Alret') {
          iconName = `alarm-light-outline`;
        } else if (routeName === 'Setting') {
          iconName = `reorder-horizontal`;
        }
        // You can return any component that you like here!

        return <IconComponent name={iconName} size={30} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: '#007AFF',
      inactiveTintColor: '#A3A3A3',
      labelStyle: {
        fontSize: 14,
      },
      style: {
        height: 60,
      },
      tabStyle: {
        backgroundColor: '#fff',
      },
    },
  }
);

export default tabNavigator;
