import { createStackNavigator } from 'react-navigation-stack';
import SelectAlretScreen from '../screens/InformHome/SelectAlretScreen';
import MapScreen from '../screens/InformHome/MapScreen';
import CompleteProcessScreen from '../screens/InformHome/CompleteProcessScreen';
import MainTabNavigator from './MainTabNavigator';

const InformHomeStack = createStackNavigator({
  App: { screen: MainTabNavigator, navigationOptions: { headerShown: false } },
  SelectAlret: SelectAlretScreen,
  Map: MapScreen,
  CompleteProcess: CompleteProcessScreen,
});

export default InformHomeStack;
