import { createStackNavigator } from 'react-navigation-stack';
import CheckHistoryScreen from '../screens/Check/CheckHistoryScreen';
import EditmeterScreen from '../screens/Check/EditmeterScreen';
import MainTabNavigator from './MainTabNavigator';

const CheckStack = createStackNavigator({
  App: { screen: MainTabNavigator, navigationOptions: { headerShown: false } },
  CheckHistory: CheckHistoryScreen,
  Editmeter: EditmeterScreen,
});

export default CheckStack;
