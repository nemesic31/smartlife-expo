import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import MainTabNavigator from './MainTabNavigator';
import RegisterStack from './RegisterNavigator';
import PaymentStack from './PaymentNavigator';
import AddmeterStack from './AddmeterNavigator';
import CheckStack from './CheckNavigator';
import InformHomeStack from './InformHomeNavigator';
import InformPublicStack from './InformPublicNavigator';
import HistoryInformStack from './HistoryInformNavigator';

const RootSwitch = createSwitchNavigator({
  Auth: RegisterStack,
  App: MainTabNavigator,
  Pay: PaymentStack,
  Addmeter: AddmeterStack,
  Check: CheckStack,
  InformHome: InformHomeStack,
  InformPublic: InformPublicStack,
  HistoryInform: HistoryInformStack,
});

const AppContainer = createAppContainer(RootSwitch);

export default AppContainer;
